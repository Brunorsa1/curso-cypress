describe("Tickets", () => {
  beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

  it("fills all the text input fields", () => {
    const firtName = "Bruno";
    const lastName = "Siqueira";

    cy.get("#first-name").type(firtName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("siqueirabruno3@gmail.com");
    cy.get("#requests").type("Carnivoro");
    cy.get("#signature").type(`${firtName} ${lastName}`);
  });

  it("Select two tickets", () => {
    cy.get("#ticket-quantity").select("2");
  });

  it("select vip ticket type", () => {
    cy.get("#vip").check()
  });

  it("selects social media checkbox", () => {
    cy.get("#social-media").check();
  });

  it("selects 'friend', and 'publication', then uncheck 'friend'", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#friend").uncheck();
  });

  it("has 'TICKETBOX header's heading", () => {
    cy.get("header h1").should("contain", "TICKETBOX")
  });

  it("alerts on invalid email", () => {
    cy.get("#email")
      .as("email")
      .type("adhsuas2mgail.com");

    cy.get("#email.invalid").should("exist");

    cy.get("@email")
      .clear()
      .type("siqueirabruno3@gmail.com");
    
      cy.get("#email.invalid").should("not.exist");
  });

  it("fills and reset the form", () => {
    const firtName = "Bruno";
    const lastName = "Siqueira";
    const fullName = `${firtName} ${lastName}`

    cy.get("#first-name").type(firtName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("siqueirabruno3@gmail.com");
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check()
    cy.get("#friend").check();
    cy.get("#requests").type("IPA Beer");

    cy.get(".agreement p").should(
      "contain",
      `I, ${fullName}, wish to buy 2 VIP tickets.`
    );

    cy.get("#agree").click();
    cy.get("#signature").type(fullName);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("button[type='reset']").click();

    cy.get("@submitButton").should("be.disabled")
  });

  it("fills mandatory fields using support command", () => {
    const customer = {
      firtName: "Joao",
      lastName: "Silva",
      email: "joaosilva@example.com"
    };

    cy.fillMandatoryFields(customer);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("#agree").uncheck();

    cy.get("@submitButton").should("be.disabled")
  });
})